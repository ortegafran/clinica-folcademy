package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    Integer id;

    @NotNull
    MedicoDto medico;
    @NotNull
    PacienteDto paciente;

    LocalDate fecha;
    LocalTime hora;
    Boolean atendido=false;
}
