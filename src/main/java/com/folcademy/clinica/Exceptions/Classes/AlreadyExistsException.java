package com.folcademy.clinica.Exceptions.Classes;

public class AlreadyExistsException extends RuntimeException{
    public AlreadyExistsException(String message) {
        super(message);
    }
}
