package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.IPacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {
    private final IPacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(IPacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    @Override
    public List<PacienteDto> findAll() {
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();

        return pacientes
                .stream()
                .map(pacienteMapper::entityDto)
                .collect(Collectors.toList());
    }

    public Page<PacienteDto> findAll(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(sortBy));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityDto);
    }

    @Override
    public PacienteDto findById(Integer id) {

        return pacienteRepository
                .findById(id)
                .map(pacienteMapper::entityDto)
                .orElse(null);
    }

    public PacienteDto save(PacienteDto dto){

        return pacienteMapper.entityDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto)
                )
        );
    }

    public PacienteDto edit(PacienteDto dto){
        if(!pacienteRepository.findById(dto.getId()).isPresent())
            return null;

        return pacienteMapper.entityDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto)
                )
            );
    }

    public PacienteDto delete(Integer id){
        Optional<Paciente> paciente = pacienteRepository.findById(id);
        if(!paciente.isPresent())
            return null;

        PacienteDto p = pacienteMapper.entityDto(paciente.get());
        pacienteRepository.deleteById(p.getId());
        return p;
    }

    public PacienteDto findByDni(String dni){
        Optional<Paciente> paciente = pacienteRepository.findByDni(dni);
        if(!paciente.isPresent()) return null;
        else return pacienteMapper.entityDto(paciente.get());
    }


}
