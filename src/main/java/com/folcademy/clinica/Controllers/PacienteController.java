package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Exceptions.Classes.AlreadyExistsException;
import com.folcademy.clinica.Exceptions.Classes.CouldntComplete;
import com.folcademy.clinica.Exceptions.Classes.NotFoundException;
import com.folcademy.clinica.Exceptions.Classes.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/pacientes")
public class PacienteController {
    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

//    @PreAuthorize("hasAuthority('pacientes_findAll')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findAll())
                ;
    }

//    @PreAuthorize("hasAuthority('pacientes_findById')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDto>> findAll(
            @RequestParam(name="pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize", defaultValue = "4") Integer pageSize,
            @RequestParam(name="orderField", defaultValue = "dni") String sortBy
    ){
        if(pageNumber<0)
            throw new ValidationException("Valor invalido. Numero de página negativo.");
        if(pageSize<0)
            throw new ValidationException("Valor invalido. Tamaño de página negativo.");

        return ResponseEntity.ok(pacienteService.findAll(pageNumber,pageSize,sortBy));
    }


    //@PreAuthorize("hasAuthority('pacientes_findById')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<PacienteDto> findById(@PathVariable(name = "id") Integer id) {
        PacienteDto p = pacienteService.findById(id);
        if(p==null)
            throw new NotFoundException("Paciente no encontrado. Id inválido.");

        return ResponseEntity
                .ok()
                .body(p);
    }

//    @PreAuthorize("hasAuthority('pacientes_addOne')")
    @PostMapping(value="")
    public ResponseEntity<PacienteDto> addOne(@RequestBody @Validated PacienteDto dto){
        if(pacienteService.findByDni(dto.getDni())!=null)
            throw new AlreadyExistsException("Paciente ya existente. Paciente no guardado.");

        PacienteDto pacienteDto = pacienteService.save(dto);

        if(pacienteDto==null)
            throw new CouldntComplete("No se logro completar la operacion.");

        return ResponseEntity.ok(pacienteDto);
    }

//    @PreAuthorize("hasAuthority('pacientes_editOne')")
    @PutMapping(value="/{id}")
    public ResponseEntity<PacienteDto> editOne(@PathVariable(name="id") Integer id,
                                               @RequestBody @Validated PacienteDto dto){
        if(pacienteService.findById(id)==null)
            throw new NotFoundException("Id de paciente no valido. No se edito nigun paciente.");

        dto.setId(id);
        if(pacienteService.edit(dto)==null)
            throw new CouldntComplete("No se logro completar la operacion.");

        return ResponseEntity.ok(dto);
    }

//    @PreAuthorize("hasAuthority('pacientes_delete')")
    @DeleteMapping(value="/{id}")
    public ResponseEntity<PacienteDto> delete(@PathVariable(name="id") Integer id){
        PacienteDto paciente = pacienteService.findById(id);

        if(paciente==null)
            throw new NotFoundException("Paciente no encontrado. No se elimino ningun paciente.");

        paciente = pacienteService.delete(id);

        if(paciente==null)
            throw new CouldntComplete("No se pudo eliminar el paciente.");

        return ResponseEntity.ok(paciente);
    }
}
